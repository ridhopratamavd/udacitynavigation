package com.example.udacitynavigation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.udacitynavigation.R
import com.example.udacitynavigation.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val contentView = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        drawerLayout = contentView.drawerLayout
        val navigationController = this.findNavController(R.id.navHostFragment)
        NavigationUI.setupActionBarWithNavController(this, navigationController, drawerLayout)
        navigationController.addOnDestinationChangedListener { controller, destination, _ ->
            if (destination.id == controller.graph.startDestination) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            } else {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            }
        }
        NavigationUI.setupWithNavController(contentView.navView, navigationController)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navigationController = this.findNavController(R.id.navHostFragment)
        return NavigationUI.navigateUp(navigationController, drawerLayout)
    }
}