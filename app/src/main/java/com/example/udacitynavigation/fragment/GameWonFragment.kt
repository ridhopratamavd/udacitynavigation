package com.example.udacitynavigation.fragment

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.udacitynavigation.fragment.GameWonFragmentArgs
import com.example.udacitynavigation.fragment.GameWonFragmentDirections
import com.example.udacitynavigation.R
import com.example.udacitynavigation.databinding.FragmentGameWonBinding

class GameWonFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGameWonBinding =
            DataBindingUtil.inflate(inflater,
                R.layout.fragment_game_won, container, false)
        setHasOptionsMenu(true)

//        Toast.makeText(
//            context,
//            "numcorrect: ${args.numCorrect} , numquestion: ${args.numQuestion}",
//            Toast.LENGTH_LONG
//        ).show()
        binding.nextMatchButton.setOnClickListener { view: View ->
            view.findNavController()
                .navigate(GameWonFragmentDirections.actionGameWonFragmentToGameFragment4())
        }
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.share_menu, menu)
    }

    private fun getShareIntent(): Intent {
        val args = GameWonFragmentArgs.fromBundle(
            requireArguments()
        )
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(
                Intent.EXTRA_TEXT,
                getString(R.string.share_text, "", args.numCorrect, args.numQuestion)
            )
        return shareIntent
    }

    private fun shareSuccess() {
        startActivity(getShareIntent())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.share -> shareSuccess()
        }
        return super.onOptionsItemSelected(item)
    }
}